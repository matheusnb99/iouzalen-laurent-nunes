package com.exo2.Exercice2.utils;

import java.security.Key;
import java.util.Date;

import com.exo2.Exercice2.dto.AuthenticationTokenDto;
import com.exo2.Exercice2.entity.Member;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;

public class JWTUtils {
    private static final Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    public static AuthenticationTokenDto generateToken(Member member) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        String token = Jwts.builder()
                .claim("member", member)
                .setSubject(member.getEmail())
                .setIssuedAt(now)
                .setExpiration(new Date(nowMillis + 30000))
                .signWith(key)
                .compact();

        AuthenticationTokenDto tokenDto = new AuthenticationTokenDto();

        tokenDto.setToken(token);
        tokenDto.setExpiresIn(30000);
        tokenDto.setIssuedAt(System.currentTimeMillis() / 1000);

        return tokenDto;
    }

    public static Member validateToken(String token) {
        try {
            Jws<Claims> claimsJws = Jwts.parserBuilder()
                    .setSigningKey(key)
                    .build()
                    .parseClaimsJws(token);
            return claimsJws.getBody().get("member", Member.class);
        } catch (SignatureException e) {
            throw new IllegalArgumentException("Invalid token");
        }
    }
}