package com.exo2.Exercice2.service;

import org.springframework.stereotype.Service;

import com.exo2.Exercice2.dto.MemberDto;
import com.exo2.Exercice2.mapper.MemberMapper;
import com.exo2.Exercice2.repository.MemberRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class MemberService {
    private final MemberRepository membersRepository;
    private final MemberMapper membersMapper;

    public MemberDto save(MemberDto membersDto) {
        return membersMapper.toDto(membersRepository.save(membersMapper.toEntity(membersDto)));
    }

    public MemberDto findById(Long id) {
        return membersMapper.toDto(membersRepository.findById(id).get());
    }

    public void deleteById(Long id) {
        membersRepository.deleteById(id);
    }

    public MemberDto update(MemberDto membersDto) {
        return membersMapper.toDto(membersRepository.save(membersMapper.toEntity(membersDto)));
    }
}
