package com.exo2.Exercice2.service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exo2.Exercice2.dto.AuthenticationTokenDto;
import com.exo2.Exercice2.dto.MemberDto;
import com.exo2.Exercice2.entity.Member;
import com.exo2.Exercice2.mapper.MemberMapper;
import com.exo2.Exercice2.repository.MemberRepository;
import com.exo2.Exercice2.utils.JWTUtils;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AuthenticationService {
    @Autowired
    private final MemberRepository memberRepository;
    @Autowired
    private final MemberMapper memberMapper;

    private final int ITERATION_COUNT = 65536;
    private final int KEY_LENGTH = 128;
    private final String ALGORITHM = "PBKDF2WithHmacSHA1";

    public AuthenticationTokenDto login(String email, String password)
            throws InvalidKeySpecException, NoSuchAlgorithmException, IllegalArgumentException {
        Member member = memberRepository.findMemberByEmail(email).orElseThrow();

        Base64.Decoder dec = Base64.getDecoder();
        byte[] salt = dec.decode(member.getPasswordSalt());

        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH);
        SecretKeyFactory f = SecretKeyFactory.getInstance(ALGORITHM);
        byte[] hash = f.generateSecret(spec).getEncoded();

        // Encode the hashed password to compare with the stored hash
        Base64.Encoder enc = Base64.getEncoder();
        String hashedPassword = enc.encodeToString(hash);

        // Compare the hashed password with the stored hash
        if (hashedPassword.equals(member.getPasswordHash())) {
            AuthenticationTokenDto token = JWTUtils.generateToken(member);

            return token;
        } else {
            throw new IllegalArgumentException("Invalid email or password");
        }
    }

    public AuthenticationTokenDto register(MemberDto member, String password)
            throws InvalidKeySpecException, NoSuchAlgorithmException {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];

        random.nextBytes(salt);

        System.out.println(member);

        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH);
        SecretKeyFactory f = SecretKeyFactory.getInstance(ALGORITHM);
        byte[] hash = f.generateSecret(spec).getEncoded();
        Base64.Encoder enc = Base64.getEncoder();

        Member memberEntity = memberMapper.toEntity(member);
        memberEntity.setPasswordHash(enc.encodeToString(hash));
        memberEntity.setPasswordSalt(enc.encodeToString(salt));

        memberRepository.save(memberEntity);

        AuthenticationTokenDto token = JWTUtils.generateToken(memberEntity);
        return token;
    }

}
