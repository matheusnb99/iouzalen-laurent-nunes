package com.exo2.Exercice2.service;

import org.springframework.stereotype.Service;

import com.exo2.Exercice2.dto.ReviewDto;
import com.exo2.Exercice2.mapper.ReviewMapper;
import com.exo2.Exercice2.repository.ReviewsRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ReviewService {
    private final ReviewsRepository reviewsRepository;
    private final ReviewMapper reviewsMapper;

    public ReviewDto save(ReviewDto reviewsDto) {
        return reviewsMapper.toDto(reviewsRepository.save(reviewsMapper.toEntity(reviewsDto)));
    }

    public ReviewDto findById(Long id) {
        return reviewsMapper.toDto(reviewsRepository.findById(id).get());
    }

    public void deleteById(Long id) {
        reviewsRepository.deleteById(id);
    }

    public ReviewDto update(ReviewDto reviewsDto) {
        return reviewsMapper.toDto(reviewsRepository.save(reviewsMapper.toEntity(reviewsDto)));
    }
}