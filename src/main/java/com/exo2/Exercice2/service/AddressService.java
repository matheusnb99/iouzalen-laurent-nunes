package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.AddressDto;
import com.exo2.Exercice2.entity.Address;
import com.exo2.Exercice2.mapper.AddressMapper;
import com.exo2.Exercice2.repository.AddressRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class AddressService {
    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;

    public AddressDto save(AddressDto addressDto) {
        return addressMapper.toDto(addressRepository.save(addressMapper.toEntity(addressDto)));
    }

    public AddressDto findById(Long id) {
        return addressMapper.toDto(addressRepository.findById(id).get());
    }

    public void deleteById(Long id) {
        addressRepository.deleteById(id);
    }

    public AddressDto update(Long id, AddressDto addressDto) {
        return addressRepository.findById(id)
                .map(existingAddress -> {
                    Address updatedAddress = addressMapper.toEntity(addressDto);

                    updatedAddress.setId(existingAddress.getId());

                    return addressMapper.toDto(addressRepository.save(updatedAddress));
                })
                .orElseThrow(() -> new RuntimeException("Address not found"));


    }



}
