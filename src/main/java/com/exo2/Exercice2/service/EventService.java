package com.exo2.Exercice2.service;

import com.exo2.Exercice2.entity.Address;
import com.exo2.Exercice2.entity.Event;
import com.exo2.Exercice2.entity.Member;
import com.exo2.Exercice2.repository.AddressRepository;
import com.exo2.Exercice2.repository.MemberRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.exo2.Exercice2.dto.EventDto;
import com.exo2.Exercice2.mapper.EventMapper;
import com.exo2.Exercice2.repository.EventRepository;

import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.data.domain.Pageable;


@Service
@AllArgsConstructor
public class EventService {
    private final EventRepository eventsRepository;
    private final EventMapper eventsMapper;
    private final AddressRepository addressRepository;
    private final MemberRepository memberRepository;


    @Caching(put = @CachePut(value = "event", key = "#eventsDto.id"), evict = @CacheEvict(value = "eventList", allEntries = true))
    public EventDto save(EventDto eventsDto) {
        Event e = eventsMapper.toEntity(eventsDto);

        if (e.getAddress() != null && e.getAddress().getId() == null) {
            Address savedAddress = addressRepository.save(e.getAddress());
            e.setAddress(savedAddress);
        }

        // if only the id is not provided, we need to save the member, else we just need to update the member
        if (eventsDto.getCreatorId() != null) {
            Member creator = memberRepository.findById(eventsDto.getCreatorId())
                    .orElseThrow(() -> new EntityNotFoundException("Creator not found with ID: " + eventsDto.getCreatorId()));
            e.setCreator(creator);
        } else {
            throw new IllegalArgumentException("Creator ID must be provided");
        }


        return eventsMapper.toDto(eventsRepository.save(e));
    }

    @CachePut(value = "event", key = "#id")
    public EventDto findById(Long id) {
        return eventsMapper.toDto(eventsRepository.findById(id).get());
    }

    @CacheEvict(value = {"event", "eventList"}, key = "#id")
    public void deleteById(Long id) {
        eventsRepository.deleteById(id);
    }




    @Caching(put = @CachePut(value = "event", key = "#id"), evict = @CacheEvict(value = "eventList", allEntries = true))
    public EventDto update(Long id, EventDto eventDto) {
        return eventsRepository.findById(id)
                .map(existingEvent -> {
                    Event event = eventsMapper.toEntity(eventDto);
                    event.setId(id);
                    if (Objects.nonNull(existingEvent.getInterestedMember())) {
                        event.setInterestedMember(existingEvent.getInterestedMember());
                    }
                    if(Objects.nonNull(existingEvent.getInvitedMember()) || existingEvent.getInvitedMember().size() != 0) {
                        event.setInvitedMember(existingEvent.getInvitedMember());
                    }
                    return eventsMapper.toDto(eventsRepository.save(event));
                })
                .orElse(null);
    }


    @Cacheable(value = "events", key = "#pageable.pageNumber + '-' + #pageable.pageSize")

    public List<EventDto> findAll(Pageable pageable) {
        return eventsRepository.findAll(pageable).map(eventsMapper::toDto).getContent();
    }

    public List<EventDto> search(String name) {
        Optional<List<Event>> events = eventsRepository.findByName(name);

        if (events.isEmpty()) {
            return null;
        }
        return events.map(eventsMapper::toDtos).get();
    }

    public EventDto subscribe(Long id, Long memberId) {
        Member member = memberRepository.findById(memberId).get();
        if (member == null) {
            return null;
        }

        return eventsRepository.findById(id)
                .map(existingEvent -> {
                    existingEvent.getInterestedMember().add(member);
                    return eventsMapper.toDto(eventsRepository.save(existingEvent));
                })
                .orElse(null);
    }

    public EventDto unsubscribe(Long id, Long memberId) {
        Member member = memberRepository.findById(memberId).get();
        if (member == null) {
            return null;
        }

        return eventsRepository.findById(id)
                .map(existingEvent -> {
                    existingEvent.getInterestedMember().remove(member);
                    return eventsMapper.toDto(eventsRepository.save(existingEvent));
                })
                .orElse(null);
    }

    public EventDto invite(Long id, Long memberId) {
        Member member = memberRepository.findById(memberId).get();
        if (member == null) {
            return null;
        }

        return eventsRepository.findById(id)
                .map(existingEvent -> {
                    existingEvent.getInvitedMember().add(member);
                    return eventsMapper.toDto(eventsRepository.save(existingEvent));
                })
                .orElse(null);
    }

    public EventDto uninvite(Long id, Long memberId) {
        Member member = memberRepository.findById(memberId).get();
        if (member == null) {
            return null;
        }

        return eventsRepository.findById(id)
                .map(existingEvent -> {
                    existingEvent.getInvitedMember().remove(member);
                    return eventsMapper.toDto(eventsRepository.save(existingEvent));
                })
                .orElse(null);
    }


}
