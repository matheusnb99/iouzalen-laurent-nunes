package com.exo2.Exercice2.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.exo2.Exercice2.dto.MemberDto;
import com.exo2.Exercice2.entity.Member;

@Mapper(componentModel = "spring")
public interface MemberMapper {
    MemberDto toDto(Member members);

    Member toEntity(MemberDto membersDto);

    List<MemberDto> toDtos(List<Member> members);

    List<Member> toEntities(List<MemberDto> membersDtos);
}
