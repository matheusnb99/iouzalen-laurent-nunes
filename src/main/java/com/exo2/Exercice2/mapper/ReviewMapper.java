package com.exo2.Exercice2.mapper;

import com.exo2.Exercice2.dto.ReviewDto;
import com.exo2.Exercice2.entity.Review;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ReviewMapper {
    ReviewDto toDto(Review reviews);

    Review toEntity(ReviewDto reviewsDto);

    List<ReviewDto> toDtos(List<Review> reviews);

    List<Review> toEntities(List<ReviewDto> reviewsDtos);
}
