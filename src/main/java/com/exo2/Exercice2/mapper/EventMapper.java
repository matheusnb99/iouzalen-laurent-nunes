package com.exo2.Exercice2.mapper;

import com.exo2.Exercice2.dto.EventDto;
import com.exo2.Exercice2.entity.Event;
import com.exo2.Exercice2.entity.Member;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EventMapper {
    EventMapper INSTANCE = Mappers.getMapper(EventMapper.class);

    @Mapping(source = "creator.id", target = "creatorId")

    EventDto toDto(Event events);
    @Mapping(source = "creatorId", target = "creator", qualifiedByName = "mapCreatorIdToMember")

    Event toEntity(EventDto eventsDto);

    List<EventDto> toDtos(List<Event> events);

    List<Event> toEntities(List<EventDto> eventsDtos);

    @Named("mapCreatorIdToMember")
    default Member mapCreatorIdToMember(Long creatorId) {
        if (creatorId == null) {
            return null;
        }
        Member member = new Member();
        member.setId(creatorId); // Assuming Member has setId method for setting ID
        return member;
    }
}
