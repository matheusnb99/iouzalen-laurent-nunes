package com.exo2.Exercice2.dto;

import java.util.Date;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY, description = "The id of the event", example = "1")
    private Long id;
    @Schema(description = "The name of the event", example = "Concert")
    private String name;
    @Schema(description = "The description of the event", example = "Concert de rock")
    private String description;
    @Schema(description = "The date of the event", example = "2021-12-31")
    private Date eventDate;
    @Schema(description = "The max capacity of the event", example = "120")
    private Integer capacity;
    @Schema(description = "The number of people that accepted going to the event", example = "100")
    private Integer attendees;
    @Schema(description = "The price of the event", example = "10.0")
    private Double price;

    @Schema(description = "The address of the event")
    private AddressDto address;
    @Schema(description = "The creator of the event")
    private Long creatorId;
}
