package com.exo2.Exercice2.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY, description = "The id of the address", example = "1")
    private Long id;
    @Schema(description = "The street of the address", example = "Rue de la paix")
    private String street;
    @Schema(description = "The city of the address", example = "Paris")
    private String city;
    @Schema(description = "The zip code of the address", example = "75000")
    private int zipCode;
    @Schema(description = "The country of the address", example = "France")
    private String country;
}
