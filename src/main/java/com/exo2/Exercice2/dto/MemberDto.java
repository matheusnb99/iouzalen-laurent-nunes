package com.exo2.Exercice2.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY, description = "The id of the member", example = "1")
    private Long id;
    @Schema(description = "The first name of the member", example = "John")
    private String firstName;
    @Schema(description = "The last name of the member", example = "Doe")
    private String lastName;
    @Schema(description = "The email of the member", example = "john.doe@example.com")
    private String email;
    @Schema(description = "The role of the member", example = "admin")
    private String role;

    @Schema(description = "The events created by the member")
    private List<EventDto> eventsCreated;
    @Schema(description = "The events the member is invited to")
    private List<EventDto> eventsInvitations;
    @Schema(description = "The events the member is interested in")
    private List<EventDto> eventsInterested;

}
