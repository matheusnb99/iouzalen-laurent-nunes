package com.exo2.Exercice2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Table(name = "reviews")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Review {
    @EmbeddedId
    ReviewId id = new ReviewId();

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("memberId")
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("eventId")
    private Event event;

    @Column(name = "rating", nullable = false)
    private Byte rating;

    @Column(name = "comment", nullable = false)
    private String comment;

    @Column(name = "publication_date", nullable = false)
    private Date publicationDate;
}
