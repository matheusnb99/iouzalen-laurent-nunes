package com.exo2.Exercice2.entity;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class ReviewId implements Serializable {
    private Long memberId;
    private Long eventId;

    @Override
        public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReviewId reviewId = (ReviewId) o;

        if (!Objects.equals(memberId, reviewId.memberId)) return false;
        return Objects.equals(eventId, reviewId.eventId);
    }

    @Override
    public int hashCode() {
        int result = memberId != null ? memberId.hashCode() : 0;
        result = 31 * result + (eventId != null ? eventId.hashCode() : 0);
        return result;
    }

}

