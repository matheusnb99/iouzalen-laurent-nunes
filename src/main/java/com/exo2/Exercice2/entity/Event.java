package com.exo2.Exercice2.entity;

import java.util.Date;
import java.util.List;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;
import org.springframework.boot.context.properties.bind.DefaultValue;

@Entity
@Table(name = "events", indexes = {
        @Index(name = "idx_eventName", columnList = "name") ,
        @Index(name = "idx_eventNameAndDate", columnList = "name, event-date")
})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_id")
    private Long id;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "description", nullable = false, length = 50)
    private String description;

    @Temporal(TemporalType.DATE)
    @Column(name = "event-date")
    private Date eventDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.DATE)
    @Column(name = "updated_at")
    private Date uptatedAt;

    @Column(name = "capacity", nullable = false)
    private Integer capacity = 10;

    @Column(name = "attendees", nullable = false)
    private Integer attendees = 0;

    @Column(name = "price", nullable = false)
    private Float price = 0f;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "adress_id")
    private Address address;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id")
    private Member creator;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "events_invited_members", joinColumns = @JoinColumn(name = "event_id"), inverseJoinColumns = @JoinColumn(name = "member_id"))
    @BatchSize(size = 20)
    private List<Member> invitedMember;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "events_interested_members", joinColumns = @JoinColumn(name = "event_id"), inverseJoinColumns = @JoinColumn(name = "member_id"))
    @BatchSize(size = 20)
    private List<Member> interestedMember;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "events_reviews", joinColumns = @JoinColumn(name = "event_id"), inverseJoinColumns = @JoinColumn(name = "member_id"))
    @BatchSize(size = 20)
    private List<Member> reviews;

}
