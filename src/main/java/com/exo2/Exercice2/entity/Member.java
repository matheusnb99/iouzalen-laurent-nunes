package com.exo2.Exercice2.entity;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;

@Entity
@Table(name = "members")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Member {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "address_id")
        private Long id;

        @Column(nullable = false, length = 50)
        private String firstName;

        @Column(nullable = false, length = 50)
        private String lastName;

        @Column(nullable = false, length = 50)
        private String email;

        @Column(nullable = false, length = 50)
        private String passwordHash;

        @Column(nullable = false, length = 50)
        private String passwordSalt;

        @Column(nullable = false, length = 50)
        private String role;

        @OneToMany(mappedBy = "creator")
        private List<Event> eventsCreated;

        @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
        @JoinTable(name = "events_invited_members", joinColumns = @JoinColumn(name = "member_id"), inverseJoinColumns = @JoinColumn(name = "event_id"))
        @BatchSize(size = 10)
        private List<Event> eventsInvitations;

        @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
        @JoinTable(name = "events_interested_members", joinColumns = @JoinColumn(name = "member_id"), inverseJoinColumns = @JoinColumn(name = "event_id"))
        @BatchSize(size = 10)
        private List<Event> eventsInterested;

        @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
        @JoinTable(name = "events_reviews", joinColumns = @JoinColumn(name = "member_id"), inverseJoinColumns = @JoinColumn(name = "event_id"))
        @BatchSize(size = 10)
        private List<Event> eventsReviews;
}
