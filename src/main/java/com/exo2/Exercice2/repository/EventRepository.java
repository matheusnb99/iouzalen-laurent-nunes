package com.exo2.Exercice2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exo2.Exercice2.entity.Event;

import java.util.List;
import java.util.Optional;

public interface EventRepository extends JpaRepository<Event, Long> {
    Optional<List<Event>> findByName(String name);

}
