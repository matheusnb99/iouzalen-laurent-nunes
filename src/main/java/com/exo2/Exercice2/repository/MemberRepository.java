package com.exo2.Exercice2.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exo2.Exercice2.entity.Member;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findMemberByEmail(String email);
}
