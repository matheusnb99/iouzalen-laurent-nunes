package com.exo2.Exercice2.controller;

import com.exo2.Exercice2.dto.EventDto;
import com.exo2.Exercice2.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/event")
public class EventController {
    @Autowired
    private EventService eventService;

    @GetMapping
    public ResponseEntity<List<EventDto>> findAll(@RequestParam(defaultValue = "0") Integer pageNo,
                                                  @RequestParam(defaultValue = "10") Integer pageSize) {
        return ResponseEntity.ok(eventService.findAll(PageRequest.of(pageNo, pageSize)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<EventDto> findById(@PathVariable Long id) {
        return ResponseEntity.ok(eventService.findById(id));
    }

    @PostMapping
    public ResponseEntity<EventDto> save(@RequestBody EventDto eventDto) {
        return ResponseEntity.ok(eventService.save(eventDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<EventDto> update(@PathVariable Long id, @RequestBody EventDto eventDto) {
        return ResponseEntity.ok(eventService.update(id, eventDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        eventService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/search")
    public ResponseEntity<List<EventDto>> search(@RequestParam String name) {
        return ResponseEntity.ok(eventService.search(name));
    }

    @PostMapping("/{id}/subscribe")
    public ResponseEntity<EventDto> subscribe(@PathVariable Long eventId, @RequestBody Long memberId) {
        return ResponseEntity.ok(eventService.subscribe(eventId, memberId));
    }

    @PostMapping("/{id}/unsubscribe")
    public ResponseEntity<EventDto> unsubscribe(@PathVariable Long eventId, @RequestBody Long memberId) {
        return ResponseEntity.ok(eventService.unsubscribe(eventId, memberId));
    }

    @PostMapping("/{id}/invite")
    public ResponseEntity<EventDto> accept(@PathVariable Long eventId, @RequestBody Long memberId) {
        return ResponseEntity.ok(eventService.invite(eventId, memberId));
    }

    @PostMapping("/{id}/refuse")
    public ResponseEntity<EventDto> refuse(@PathVariable Long eventId, @RequestBody Long memberId) {
        return ResponseEntity.ok(eventService.uninvite(eventId, memberId));
    }

}
