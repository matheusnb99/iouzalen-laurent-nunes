package com.exo2.Exercice2.controller;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exo2.Exercice2.dto.AuthenticationTokenDto;
import com.exo2.Exercice2.dto.RegisterDto;
import com.exo2.Exercice2.service.AuthenticationService;

@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseEntity<AuthenticationTokenDto> login(@RequestBody Map<String, Object> data) {
        try {
            return ResponseEntity
                    .ok(authenticationService.login((String) data.get("email"), (String) data.get("password")));
        } catch (IllegalArgumentException | InvalidKeySpecException | NoSuchAlgorithmException e) {
            if (e instanceof IllegalArgumentException) {
                return ResponseEntity.status(Response.SC_UNAUTHORIZED).build();
            }

            e.printStackTrace();
            return ResponseEntity.status(Response.SC_INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/register")
    public ResponseEntity<AuthenticationTokenDto> register(@RequestBody RegisterDto data) {
        System.out.println();
        System.out.println(data);

        try {
            return ResponseEntity.ok(authenticationService.register(data.getMember(), data.getPassword()));
        } catch (IllegalArgumentException | InvalidKeySpecException | NoSuchAlgorithmException e) {
            if (e instanceof IllegalArgumentException) {
                return ResponseEntity.status(Response.SC_BAD_REQUEST).build();
            }

            e.printStackTrace();
            return ResponseEntity.status(Response.SC_INTERNAL_SERVER_ERROR).build();
        }
    }

}
